package com.example.futbet.ui.ligas;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class LigasViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public LigasViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is ligas fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}