package com.example.futbet.ui.ligas;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.futbet.databinding.FragmentLigasBinding;

public class LigasFragment extends Fragment {

    private LigasViewModel ligasViewModel;
    private FragmentLigasBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        ligasViewModel =
                new ViewModelProvider(this).get(LigasViewModel.class);

        binding = FragmentLigasBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}